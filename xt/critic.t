#!/usr/bin/env perl

# Test that the module passes perlcritic

use autodie;
use strict;
use utf8::all;
use v5.20;
use warnings;

use Perl::Critic;
use Test::Perl::Critic;

all_critic_ok( qw( script/git-locust ) );

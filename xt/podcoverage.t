#!/usr/bin/env perl

# Ensure pod coverage in your distribution

use autodie;
use strict;
use utf8::all;
use v5.20;
use warnings;

use Test::Pod::Coverage;

all_pod_coverage_ok();

#!/usr/bin/env perl

# Test that the module MANIFEST is up-to-date

use autodie;
use strict;
use utf8::all;
use v5.20;
use warnings;

use Test::DistManifest;

manifest_ok();

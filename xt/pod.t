#!/usr/bin/env perl

# Test that the syntax of our POD documentation is valid

use autodie;
use strict;
use utf8::all;
use v5.20;
use warnings;

use Pod::Simple;
use Test::Pod;

all_pod_files_ok();
